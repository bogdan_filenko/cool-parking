using System.Collections.Generic;
using CoolParking.BL.Models;

namespace CoolParking.BL.Interfaces
{
    public interface ITransactionsService
    {
        List<TransactionInfo> ParkingTransactions { get; }
        void AddTransactionInfo(TransactionInfo transactionInfo);
        void ClearTransactionsList();
    }
}