﻿using System;
using CoolParking.BL.Models;
using System.Linq;
using CoolParking.BL.Interfaces;
using System.Collections.ObjectModel;
using System.Timers;
using System.Text;
using static CoolParking.BL.Models.Settings;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private Parking _parking;
        private readonly ILogService _logService;
        private readonly ITimerService _withdrawTimer;
        private readonly ITimerService _logTimer;
        private readonly ITransactionsService _transactionsService;
        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _parking = Parking.GetInstance();
            _withdrawTimer = withdrawTimer;
            _withdrawTimer.Elapsed += _WithdrawVehiclesBalances;
            
            _logTimer = logTimer;
            _logTimer.Elapsed += _LogLastParkingTransactions;
            _logService = logService;
            
            _withdrawTimer.Start();
            _logTimer.Start();

            _transactionsService = new TransactionsService();
        }
        public decimal GetBalance()
        {
            return _parking.Balance;
        }
        public int GetCapacity()
        {
            return _parking.Capacity;
        }
        public int GetFreePlaces()
        {
            return _parking.Capacity - _parking.parkedVehicles.Count;
        }
        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(_parking.parkedVehicles);
        }
        public void AddVehicle(Vehicle vehicle)
        {
            if (_parking.parkedVehicles.Find(v => v.Id == vehicle.Id) != null)
            {
                throw new ArgumentException("The vehicle with the same id is already exist");
            }
            if (GetFreePlaces() == 0)
            {
                throw new InvalidOperationException("No free places");
            }
            _parking.parkedVehicles.Add(vehicle);
        }
        public void RemoveVehicle(string vehicleId)
        {
            Vehicle vehicle = _parking.parkedVehicles.FirstOrDefault(vehicle => vehicle.Id == vehicleId);
            if (vehicle == null)
            {
                throw new ArgumentException("The vehicle does not exist");
            }
            if (vehicle.Balance < 0)
            {
                throw new InvalidOperationException("The vehicle has unpaid bills");
            }
            _parking.parkedVehicles.Remove(vehicle);
        }
        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum < 0)
            {
                throw new ArgumentException("The sum cannot be negative");
            }
            Vehicle vehicle = _parking.parkedVehicles.FirstOrDefault(vehicle => vehicle.Id == vehicleId);
            if (vehicle == default)
            {
                throw new ArgumentException("The vehicle does not exist");
            }
            vehicle.Balance += sum;
        }
        public TransactionInfo[] GetLastParkingTransactions()
        {
            return _transactionsService.ParkingTransactions.ToArray();
        }
        private void _WithdrawVehiclesBalances(object sender, ElapsedEventArgs e)
        {
            foreach (var vehicle in GetVehicles())
            {
                float tariff = 0f;
                switch (vehicle.VehicleType)
                {
                    case VehicleType.Motorcycle:
                        tariff = MotorcicleTariff;
                        break;
                    case VehicleType.Bus:
                        tariff = BusTariff;
                        break;
                    case VehicleType.PassengerCar:
                        tariff = PassengerCarTariff;
                        break;
                    case VehicleType.Truck:
                        tariff = TruckTariff;
                        break;
                }
                decimal sum = 0;
                if (vehicle.Balance < (decimal)tariff)
                {
                    if (vehicle.Balance <= 0)
                    {
                        sum = (decimal)tariff * (decimal)FineRation;
                    }
                    else
                    {
                        sum = vehicle.Balance + ((decimal)tariff - vehicle.Balance) * (decimal)FineRation;
                    }
                }
                else
                {
                    sum = (decimal)tariff;
                }
                _parking.Balance += sum;
                vehicle.Balance -= sum;
                _transactionsService.AddTransactionInfo(new TransactionInfo(sum, DateTime.Now, vehicle.Id));
            }
        }
        private void _LogLastParkingTransactions(object sender, ElapsedEventArgs e)
        {
            StringBuilder logBuilder = new StringBuilder();
            foreach (var transactionInfo in GetLastParkingTransactions())
            {
                logBuilder.Append("\t" + LogService.CreateTransactionLogInfo(transactionInfo) + "\n");
            }
            _logService.Write(logBuilder.ToString());
            _transactionsService.ClearTransactionsList();
        }
        public string ReadFromLog()
        {
            return _logService.Read();
        }
        public void Dispose()
        {
            _parking.Dispose();
            _logTimer.Dispose();
            _withdrawTimer.Dispose();
        }
    }
}