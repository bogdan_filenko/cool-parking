﻿using System.Timers;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        public event ElapsedEventHandler Elapsed;
        private Timer _timer;
        public double Interval { get; set; }
        public TimerService(double interval)
        {
            Interval = interval;
            _timer = new Timer(Interval * 1000);
        }
        public void Start()
        {
            _timer.Elapsed += Elapsed;
            _timer.AutoReset = true;
            _timer.Enabled = false;
            
            _timer.Start();
        }
        public void Stop()
        {
            _timer.Stop();
        }
        public void Dispose()
        {
            Interval = 0;
            Stop();
            _timer.Elapsed -= Elapsed;
            Elapsed = null;
            _timer = null;
        }
    }
}