﻿using CoolParking.BL.Interfaces;
using System.IO;
using CoolParking.BL.Models;
using System;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get; set; }
        public LogService(string logPath)
        {
            LogPath = logPath;
        }
        public void Write(string logInfo)
        {
            using (var writer = new StreamWriter(LogPath, true))
            {
                writer.WriteLine(logInfo);
            }
        }
        public static string CreateTransactionLogInfo(TransactionInfo transactionInfo)
        {
            return $"{transactionInfo.Date.ToString()}: VehicleId --- {transactionInfo.VehicleId}; Sum --- {transactionInfo.Sum}";
        }
        public string Read()
        {
            try
            {
                using (var reader = new StreamReader(LogPath))
                {
                    return reader.ReadToEnd();
                }
            }
            catch
            {
                throw new InvalidOperationException("The file Transactions.log is not found");
            }
        }
    }
}