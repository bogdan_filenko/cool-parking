using System.Collections.Generic;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class TransactionsService : ITransactionsService
    {
        public List<TransactionInfo> ParkingTransactions { get; }
        public TransactionsService()
        {
            ParkingTransactions = new List<TransactionInfo>();
        }
        public void AddTransactionInfo(TransactionInfo transactionInfo)
        {
            ParkingTransactions.Add(transactionInfo);
        }
        public void ClearTransactionsList()
        {
            ParkingTransactions.Clear();
        }
    }
}