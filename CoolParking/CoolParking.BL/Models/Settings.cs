﻿
namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public const int ParkingInitialBalance = 0;
        public const int ParkingCapacity = 10;
        public const int WriteOffPeriod = 5;
        public const int LogWritePeriod = 60;
        public const float FineRation = 2.5f;
        public const int PassengerCarTariff = 2;
        public const int TruckTariff = 5;
        public const float BusTariff = 3.5f;
        public const int MotorcicleTariff = 1;
        public const string VehicleIdTemplate = "XX-YYYY-XX";
    }
}