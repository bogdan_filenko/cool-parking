﻿using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public decimal Sum { get; set; }
        public DateTime Date { get; set; }
        public string VehicleId { get; set; }
        public TransactionInfo(decimal sum, DateTime date, string vehicleId)
        {
            Sum = sum;
            Date = date;
            VehicleId = vehicleId;
        }
    }
}
