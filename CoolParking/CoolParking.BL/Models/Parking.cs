﻿using System.Collections.Generic;
using System;
using static CoolParking.BL.Models.Settings;

namespace CoolParking.BL.Models
{
    public class Parking : IDisposable
    {
        private static Parking _parkingInstance;
        public decimal Balance { get; set; }
        public List<Vehicle> parkedVehicles;
        public int Capacity { get; set; }
        private Parking()
        {
            Balance = ParkingInitialBalance;
            parkedVehicles = new List<Vehicle>();
            Capacity = ParkingCapacity;
        }
        public static Parking GetInstance()
        {
            if (_parkingInstance == null)
            {
                _parkingInstance = new Parking();
            }
            return _parkingInstance;
        }
        public void Dispose()
        {
            Balance = 0;
            Capacity = 0;
            parkedVehicles.Clear();
            _parkingInstance = null;
        }
    }
}