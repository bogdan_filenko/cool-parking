﻿
namespace CoolParking.BL.Models
{
    public enum VehicleType : byte 
    {
        PassengerCar,
        Truck,
        Bus,
        Motorcycle
    }
}