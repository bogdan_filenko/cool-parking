﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using static CoolParking.BL.Models.Settings;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; init; }
        public VehicleType VehicleType { get; init; }
        public decimal Balance { get; set; }
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            if (!VehicleValidator.Validate(id, balance))
            {
                throw new ArgumentException("Id or balance argument is invalid");
            }
            this.Id = id;
            this.VehicleType = vehicleType;
            this.Balance = balance;
        }
        public static string GenerateRandomRegistrationPlateNumber()
        {
            StringBuilder vehicleIdBuilder = new StringBuilder();
            Random random = new Random();
            foreach (var letter in VehicleIdTemplate)
            {
                if (letter == 'X')
                {
                    vehicleIdBuilder.Append(Convert.ToChar(65 + random.Next(0, 25)));
                }
                else if (letter == 'Y')
                {
                    vehicleIdBuilder.Append(random.Next(0, 9));
                }
                else
                {
                    vehicleIdBuilder.Append(letter);
                }
            }
            return vehicleIdBuilder.ToString();
        }
    }
    public class VehicleValidator
    {
        public static bool Validate(string id, decimal balance)
        {
            Regex regex = new Regex(@"^[A-Z]{2}-\d{4}-[A-Z]{2}$");
            if (regex.IsMatch(id) && balance >= 0)
            {
                return true;
            }
            return false;
        }
    }
}