using CoolParking.BL.Services;
using CoolParking.Interface.Interfaces;
using static CoolParking.BL.Models.Settings;
using CoolParking.BL.Interfaces;
using System.Linq;
using System;
using System.Text;
using CoolParking.BL.Models;

namespace CoolParking.Interface.Modules
{
    public class ParkingController : IParkingController
    {
        private readonly IParkingService _parkingService;
        public event Action<string> Notify;
        public ParkingController()
        {
            ILogService logService = new LogService(".\\..\\Transactions.log");
            ITimerService withdrawTimer = new TimerService(WriteOffPeriod);
            ITimerService logTimer = new TimerService(LogWritePeriod);
            _parkingService = new ParkingService(withdrawTimer, logTimer, logService);
        } 
        public decimal GetCurrentParkingBalance()
        {
            return _parkingService.GetBalance();
        }
        public decimal GetCurrentProfit()
        {
            TransactionInfo[] lastTransactions = _parkingService.GetLastParkingTransactions();
            return lastTransactions.Aggregate(0.0m, (x, y) => x + y.Sum);
        }
        public (int, int) GetFreePlacesWithTotalCapacity()
        {
            return (_parkingService.GetFreePlaces(), _parkingService.GetCapacity());
        }
        public string GetTransactions()
        {
            StringBuilder transactionsInfoBuilder = new StringBuilder("Current period`s transactions:\n");
            foreach (var transaction in _parkingService.GetLastParkingTransactions())
            {
                transactionsInfoBuilder.Append($"\t{LogService.CreateTransactionLogInfo(transaction)}\n");
            }
            return transactionsInfoBuilder.ToString();
        }
        public string GetAllTransactionsHistory()
        {
            return _parkingService.ReadFromLog();
        }
        public string GetParkedVehicles()
        {
            StringBuilder vehiclesListBuilder = new StringBuilder("Parked vehicles:\n");
            foreach (var vehicle in _parkingService.GetVehicles())
            {
                string vehicleType = "";
                switch (vehicle.VehicleType)
                {
                    case VehicleType.Bus:
                        vehicleType = "Bus";
                        break;
                    case VehicleType.Motorcycle:
                        vehicleType = "Motorcycle";
                        break;
                    case VehicleType.PassengerCar:
                        vehicleType = "Passenger car";
                        break;
                    case VehicleType.Truck:
                        vehicleType = "Truck";
                        break;
                    default:
                        throw new ArgumentException("Unknown vehicle type!");
                }
                vehiclesListBuilder.Append($"\t{vehicleType}: {vehicle.Id} --- {vehicle.Balance}\n");
            }
            return vehiclesListBuilder.ToString();
        }
        public void AddVehicle(VehicleType vehicleType, decimal balance)
        {
            try
            {
                string vehicleId = Vehicle.GenerateRandomRegistrationPlateNumber();
                while (_parkingService.GetVehicles().FirstOrDefault(v => v.Id == vehicleId) != default)
                {
                vehicleId = Vehicle.GenerateRandomRegistrationPlateNumber(); 
                }
                Vehicle vehicle = new Vehicle(vehicleId, vehicleType, balance);
                _parkingService.AddVehicle(vehicle);
                Notify?.Invoke(vehicle.Id);   
            }
            catch (Exception ex)
            {
                Notify = null;
                throw ex;
            }
        }
        public void RemoveVehicle(string vehicleId)
        {
            try
            {
                _parkingService.RemoveVehicle(vehicleId);
                Notify?.Invoke(vehicleId);
            }
            catch (Exception ex)
            {
                Notify = null;
                throw ex;
            }
        }
        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            try
            {
                _parkingService.TopUpVehicle(vehicleId, sum);
                Notify?.Invoke(vehicleId);
            }
            catch (Exception ex)
            {
                Notify = null;
                throw ex;
            }
        }
    }
}