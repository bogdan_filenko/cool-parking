using CoolParking.BL.Models;

namespace CoolParking.Interface.Interfaces
{
    public interface IParkingController
    {
        decimal GetCurrentParkingBalance();
        decimal GetCurrentProfit();
        (int, int) GetFreePlacesWithTotalCapacity();
        string GetTransactions();
        string GetAllTransactionsHistory();
        string GetParkedVehicles();
        void AddVehicle(VehicleType vehicleType, decimal balance);
        void RemoveVehicle(string vehicleId);
        void TopUpVehicle(string vehicleId, decimal balance);
    }
}