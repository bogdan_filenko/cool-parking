
namespace CoolParking.Interface.Interfaces
{
    public interface IMenu
    {
        void ShowMainMenu();
        void ShowCurrentParkingBalance();
        void ShowCurrentProfit();
        void ShowFreePlaces();
        void ShowTransactions();
        void ShowAllTransactionsHistory();
        void ShowParkedVehicles();
        void ShowNewVehicleMenu();
        void ShowRemovingMenu();
        void ShowToppingUpMenu();
        void NotifyVehicleAdded(string vehicleId);
        void NotifyVehicleRemoved(string vehicleId);
        void NotifyVehicleBalanceToppedUp(string vehicleId);
    }
}